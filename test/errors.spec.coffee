{
  RequestError
  Forbidden
  Unauthorized
  BadRequest
  NotFound
  ServerError
} = require '../lib/errors'

describe 'lib/error', ->
  describe 'RequestError', ->
    it 'should inherit from Error', ->
      (new RequestError instanceof Error)
        .should.be.true

    describe 'Forbidden Error', ->
      it 'should inherit from Error', ->
        (new Forbidden instanceof Error)
      it 'should inherit from RequestError', ->
        (new Forbidden instanceof RequestError)

    describe 'Unauthorized Error', ->
      it 'should inherit from Error', ->
        (new Unauthorized instanceof Error)
      it 'should inherit from RequestError', ->
        (new Unauthorized instanceof RequestError)

    describe 'BadRequest Error', ->
      it 'should inherit from Error', ->
        (new BadRequest instanceof Error)
      it 'should inherit from RequestError', ->
        (new BadRequest instanceof RequestError)

    describe 'NotFound Error', ->
      it 'should inherit from Error', ->
        (new NotFound instanceof Error)
      it 'should inherit from RequestError', ->
        (new NotFound instanceof RequestError)
