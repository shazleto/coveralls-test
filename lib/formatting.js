const _ = require('lodash');

const rethrowError = (fnName, e) => {
  let formattedError;
  if (_.isPlainObject(e.error) || _.isArray(e.error)) {
    formattedError = `\n${JSON.stringify(e.error, null, 2)}`;
  } else {
    formattedError = e.error;
  }
  return `${fnName} - ${e.name}: ${e.statusCode} - ${formattedError}`;
};

const sqlEscapeWildcard = (str) => str.replace(/_|%|\\/g, (x) => `\\${x}`);

module.exports = {
  rethrowError,
  sqlEscapeWildcard,
};
